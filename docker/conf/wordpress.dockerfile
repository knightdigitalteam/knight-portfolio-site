FROM wordpress:latest

RUN pecl install xdebug

# Enabling Apache rewrite
RUN echo "     \033[1;107;90m        Enabling Rewrite and restarting Apache        \033[0;49;39m"
RUN a2enmod rewrite
RUN service apache2 restart
