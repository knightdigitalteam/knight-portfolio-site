<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<footer class="footer-container">
	<div class="footer-grid">

		<div class="spotflex">
			<div class="footlogo">
			<img src="<?php bloginfo('stylesheet_directory');?>/dist/assets/images/logopiece.svg">
			</div>
			<div class="footcontact">
				<p>
				130 S Orange Avenue<br>
				Suite N&deg; 150<br>
				Orlando FL 32801
				</p>
				<p>
				407.206.1011<br>
				<a href="mailto:Hi@KnightAgency.com">Hi@KnightAgency.com</a>
				</p>
			</div>
		</div>

		<div class="footform">
			<h3>Let's do this thing.</h3>

		</div>

		<?php dynamic_sidebar( 'footer-widgets' ); ?>
	</div>
</footer>

<footer class="colophon ltblackbg">
	<div class="grid-container">
		<div class="grid-x">
			<div class="medium-6 cell small-12 text-center medium-text-left">
				<a href="#">Privacy Policy</a> | <a href="#">Terms & Conditions</a>
			</div>
			<div class="medium-6 cell small-12 text-center medium-text-right">
				&copy;2019 Knight Agency, All Rights reserved
			</div>
		</div>
	</div>
</footer>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	</div><!-- Close off-canvas content -->
<?php endif; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>
