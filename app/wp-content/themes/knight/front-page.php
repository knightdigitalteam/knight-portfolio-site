<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<section id="homehero" class="ltblackbg">
	<div class="grid-container">
		<div class="grid-x">
		<div class="cell small-12 medium-6">
			<h5 class="fadeInLeft animated delay-1s">Long Story Short</h5>
			<h1 class="serif fadeInUp animated delay-2s">Brand Marketing for people-first organizations.</h1>
		</div>
		</div>
	</div>
</section>
<section style="padding-top:3000px;">
<h2 class="animation-element">WHOA!</h2>
</section>

<?php // get_template_part( 'template-parts/featured-image' ); ?>

<?php
get_footer();
